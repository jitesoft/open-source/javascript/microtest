# MicroTest

No framework test framework!

This is actually just a few methods throwing errors in case of test failures. No test counting, no coverage
no nothing, just test!

## Why?

During automated client-side testing, we required a minimal test helper to throw errors on failures.  

## What?
  
MicroTest exposes the following methods on the `MicroTest` object:

```javascript
throws
throwsSync
notThrows
notThrowsSync
isType
isInstance
isEqual
```

There are three files you may use to test:

* `dist/index.js`, built to run on latest JS engines (latest 1 version of chrome and firefox).
* `dist/index-compat.js`, built to run on most modern browsers.
* `src/index.js`, original source. 

## When?

June 8'th 4:30 PM. Not before, not after.

# How?

If including it as source via `import`, use the exported methods right away:

```javascript
import {throws} from '@jitesoft/microtest';
await throws(() => {
  throw new Error('Error plx!');
})
```

If using one of the dist files, the `MicroTest` object is populated with all of the methods:

```javascript
await MicroTest.throws(() => {
  throw new Error('Error plx!');
});
```

