import { throws, throwsSync, notThrowsSync, notThrows } from '../src';

describe('Test throws', () => {
  test('Throws on no exception.', async () => {
    const fn = () => {
      return throws(() => Promise.resolve(), 'Message...', '');
    };
    await expect(fn()).rejects.toThrow('Failure: Expected exception. No exception was thrown.');
  });

  test('Throws on incorrect exception message.', async () => {
    const fn = () => {
      return throws(() => Promise.reject(new Error('Hi!')), 'Message...', []);
    };
    await expect(fn()).rejects.toThrow('Failure: Expected exception with message "Message...". Got "Hi!".');
  });

  test('Throws on no exception - Sync.', () => {
    expect(() => {
      throwsSync(() => Promise.resolve(), 'Message...', []);
    }).toThrow('Failure: Expected exception. No exception was thrown.');
  });

  test('Throws on incorrect exception message - Sync.', () => {
    expect(() => {
      throwsSync(() => throw new Error('ERROR!'), 'Message...', []);
    }).toThrow('Failure: Expected exception with message "Message...". Got "ERROR!".');
  });

  test('Does nothing if exception is thrown.', async () => {
    const fn = () => {
      return throws(() => Promise.reject(new Error('Hi!')));
    };
    await expect(fn()).resolves.not.toThrow();
  });

  test('Does nothing if exception is thrown - Sync.', () => {
    expect(() => {
      return throwsSync(() => throw new Error('Hi!'));
    }).not.toThrow();
  });
});

describe('Test notThrows', () => {
  test('Throws on exception.', async () => {
    const fn = async () => {
      return notThrows(async () => throw new Error('Error!'));
    };

    await expect(fn()).rejects.toThrow('Failure: Expected no exception. Got: "Error!".');
  });

  test('Throws on exception - Sync.', () => {
    expect(() => notThrowsSync(() => { throw new Error('Error!'); })).toThrow('Failure: Expected no exception. Got: "Error!".');
  });

  test('Does nothing if no exception.', async () => {
    const fn = async () => {
      return notThrows(async () => {
        return 'abc123';
      });
    };
    await expect(fn()).resolves.toBe('abc123');
  });

  test('Does nothing if no exception - Sync.', () => {
    expect(notThrowsSync(() => 'abc123')).toBe('abc123');
  });
});
