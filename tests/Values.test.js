import { isEqual } from '../src';

describe('Test isEqual.', () => {
  test('Throws if not equal.', () => {
    expect(() => {
      isEqual('aaaa', 123123);
    }).toThrow('Failure: Value was not equal to the expected value.');
  });

  test('Throws if not strict equal.', () => {
    expect(() => {
      isEqual('123', 123, true);
    }).toThrow('Failure: Value was not equal to the expected value.');
  });

  test('Does not throw if strict equal.', () => {
    expect(() => {
      isEqual('abc', 'abc');
    }).not.toThrow();
  });

  test('Does not throw if equal.', () => {
    expect(() => {
      isEqual('123', 123, false);
    }).not.toThrow();
  });
});
