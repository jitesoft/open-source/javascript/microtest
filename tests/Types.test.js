import { isInstance, isType } from '../src';

describe('Tests for isType.', () => {
  test('Throws if not same type.', () => {
    expect(() => {
      isType('abc123', 'number');
    }).toThrow('Failure: Variable was not of expected type. Expected "string", got "number".');
  });

  test('Does not throw if same type.', () => {
    expect(() => {
      isType('abc', 'string');
    }).not.toThrow();
  });
});

describe('Tests for isInstance.', () => {
  test('Throws if not same instance type.', () => {
    expect(() => {
      isInstance('abc123', Number);
    }).toThrow('Failure: Value was not of expected type. Expected "Number", got "String".');
  });

  test('Does not throw if same instance type.', () => {
    expect(() => {
      isInstance('abc', String);
    }).not.toThrow();
  });
});
