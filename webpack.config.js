const Path = require('path');
module.exports = [{
  name: 'Support.',
  mode: process.env.NODE_ENV,
  optimization: {
    minimize: process.env.NODE_ENV === 'production'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: [/node_modules/],
        loader: 'babel-loader'
      }
    ]
  },
  output: {
    filename: 'index-compat.js',
    library: 'MicroTest',
    libraryTarget: 'window',
    globalObject: 'window'
  },
  entry: {
    index: [
      Path.join(__dirname, 'src', 'index.js')
    ]
  }
}, {
  name: 'Bleeding Edge!',
  mode: process.env.NODE_ENV,
  optimization: {
    minimize: process.env.NODE_ENV === 'production'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: [/node_modules/],
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              [
                '@jitesoft/main',
                {
                  useBuiltIns: 'entry',
                  modules: false,
                  targets: 'last 1 Chrome version, last 1 Firefox version'
                }
              ]
            ]
          }
        }
      }
    ]
  },
  output: {
    filename: 'index.js',
    library: 'MicroTest',
    libraryTarget: 'window',
    globalObject: 'window'
  },
  entry: {
    index: [
      Path.join(__dirname, 'src', 'index.js')
    ]
  }
}];
