/**
 * Resolves if function throws an error. If error message is supplied, the message will be checked with strict equality.
 *
 * @example
 * <pre>
 *   await MicroTest.throws(() => {
 *     throw Error('abc');
 *   }, 'Test!'); // Will fail with a `Failure: expected exception with message "Test!". Got "abc".`
 *
 *   await MicroTest.throws(async (arg1, arg2) => {
 *     await Something(arg1, arg2);
 *   }, null, arg1, arg2); // Will fail with a `Failure: Expected exception. No exception was thrown.`
 * </pre>
 *
 * @param {function}    test      Method to test.
 * @param {string|null} [message] Message to test.
 * @param {...*}        [args]    Arguments to pass to the test function.
 * @return {Promise<void>}
 */
declare function throws(test: () => void, message: string, ...args: any[]): Promise<void>;

/**
 * Resolves if function throws an error. If error message is supplied, the message will be checked with strict equality.
 * This method is running sync, use `throws` for async method.
 *
 * @see throws
 * @param {function}    test      Method to test.
 * @param {string|null} [message] Message to test.
 * @param {...*}        [args]    Arguments to pass to the test function.
 * @return {void}
 */
declare function throwsSync(test: () => void, message: string, ...args: any[]): void;

/**
 * Resolves if function does not throw an error. Returns value returned from the test function.
 *
 * @example
 * <pre>
 * await MicroTest.notThrow(() => {
 *   throw new Error('abc');
 * }); // Faills with: `Failure: Expected no exception. Got "abc".`
 * </pre>
 * @param {function} test   Function to invoke.
 * @param {...*}     [args] Optional arguments to pass to test function.
 * @return {Promise<*>}
 */
declare function notThrows(test: () => void, ...args: any[]): Promise<any>;

/**
 * Resolves if function does not throw an error. Returns value returned from the test function.
 * This method is running sync, use `notThrows` for async method.
 *
 * @see notThrows
 * @param {function} test   Function to invoke.
 * @param {...*}     [args] Optional arguments to pass to test function.
 * @return {*}
 */
declare function notThrowsSync(test: () => void, ...args: any[]): any;

/**
 * Test a value using typeof against a given type.
 *
 * @param {*}      test Value to test.
 * @param {String} type Type to expect.
 */
declare function isType(test: any, type: string): void;

/**
 * Test a value using instanceof against a given type.
 *
 * @param {*} test Value to test.
 * @param {*} type Instance type to test against.
 */
declare function isInstance(test: any, type: any): void;

/**
 * Test if two values are equal, either by strict or sloppy comparision, depending
 * on the last argument.
 *
 * @param {*}       value    Value to test.
 * @param {*}       expected Expected value to compare with.
 * @param {boolean} strict   If strict or sloppy compare should be used (=== vs ==).
 */
declare function isEqual(value: any, expected: any, strict?: boolean): void;

/**
 * Error thrown on assertion errors.
 */
declare class AssertionError extends Error {}

export { AssertionError, throws, throwsSync, notThrows, notThrowsSync, isInstance, isType, isEqual };
