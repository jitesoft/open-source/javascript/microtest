/* eslint-disable eqeqeq */
import { AssertionError } from './Errors';

/**
 * Test if two values are equal, either by strict or sloppy comparision, depending
 * on the last argument.
 *
 * @param {*}       value    Value to test.
 * @param {*}       expected Expected value to compare with.
 * @param {boolean} strict   If strict or sloppy compare should be used (=== vs ==).
 */
export const isEqual = (value, expected, strict = true) => {
  // noinspection EqualityComparisonWithCoercionJS
  if ((strict && expected !== value) || (!strict && value != expected)) {
    throw new AssertionError('Failure: Value was not equal to the expected value.');
  }
};
