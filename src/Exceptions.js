import { AssertionError } from './Errors';

/**
 * Resolves if function throws an error. If error message is supplied, the message will be checked with strict equality.
 *
 * @example
 * <pre>
 *   await MicroTest.throws(() => {
 *     throw Error('abc');
 *   }, 'Test!'); // Will fail with a `Failure: expected exception with message "Test!". Got "abc".`
 *
 *   await MicroTest.throws(async (arg1, arg2) => {
 *     await Something(arg1, arg2);
 *   }, null, arg1, arg2); // Will fail with a `Failure: Expected exception. No exception was thrown.`
 * </pre>
 *
 * @param {function}    test      Method to test.
 * @param {string|null} [message] Message to test.
 * @param {...*}        [args]    Arguments to pass to the test function.
 * @return {Promise<void>}
 */
export const throws = async (test, message = null, ...args) => {
  try {
    await test(...args);
  } catch (ex) {
    if (message && message !== ex.message) {
      throw new AssertionError(`Failure: Expected exception with message "${message}". Got "${ex.message}".`);
    }
    return;
  }
  throw new AssertionError('Failure: Expected exception. No exception was thrown.');
};

/**
 * Resolves if function throws an error. If error message is supplied, the message will be checked with strict equality.
 * This method is running sync, use `throws` for async method.
 *
 * @see throws
 * @param {function}    test      Method to test.
 * @param {string|null} [message] Message to test.
 * @param {...*}        [args]    Arguments to pass to the test function.
 * @return {void}
 */
export const throwsSync = (test, message = null, ...args) => {
  try {
    test(...args);
  } catch (ex) {
    if (message && message !== ex.message) {
      throw new AssertionError(`Failure: Expected exception with message "${message}". Got "${ex.message}".`);
    }
    return;
  }
  throw new AssertionError('Failure: Expected exception. No exception was thrown.');
};

/**
 * Resolves if function does not throw an error. Returns value returned from the test function.
 *
 * @example
 * <pre>
 * await MicroTest.notThrow(() => {
 *   throw new Error('abc');
 * }); // Faills with: `Failure: Expected no exception. Got "abc".`
 * </pre>
 * @param {function} test   Function to invoke.
 * @param {...*}     [args] Optional arguments to pass to test function.
 * @return {Promise<*>}
 */
export const notThrows = async (test, ...args) => {
  try {
    return await test(...args);
  } catch (ex) {
    throw new AssertionError(`Failure: Expected no exception. Got: "${ex.message}".`);
  }
};

/**
 * Resolves if function does not throw an error. Returns value returned from the test function.
 * This method is running sync, use `notThrows` for async method.
 *
 * @see notThrows
 * @param {function} test   Function to invoke.
 * @param {...*}     [args] Optional arguments to pass to test function.
 * @return {any}
 */
export const notThrowsSync = (test, ...args) => {
  try {
    return test(...args);
  } catch (ex) {
    throw new AssertionError(`Failure: Expected no exception. Got: "${ex.message}".`);
  }
};
