import { AssertionError } from './Errors';

/**
 * Test a value using typeof against a given type.
 *
 * @param {*}      test Value to test.
 * @param {String} type Type to expect.
 */
export const isType = (test, type) => {
  // eslint-disable-next-line valid-typeof
  if (typeof test !== type) {
    throw new AssertionError(`Failure: Variable was not of expected type. Expected "${typeof test}", got "${type}".`);
  }
};

/**
 * Test a value using instanceof against a given type.
 *
 * @param {*} test Value to test.
 * @param {*} type Instance type to test against.
 */
export const isInstance = (test, type) => {
  if (test.constructor === type) {
    return;
  }
  throw new AssertionError(`Failure: Value was not of expected type. Expected "${type.name}", got "${test.constructor.name}".`);
};
